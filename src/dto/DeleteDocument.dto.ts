import { MaxLength, IsNotEmpty, IsEmail, IsString, Matches, IsNumber } from 'class-validator';
export class DeleteDocumentDto {
    
    @IsString()
    @IsNotEmpty()
    readonly id

}