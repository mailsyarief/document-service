import { MaxLength, IsNotEmpty, IsEmail, IsString, Matches, IsNumber } from 'class-validator';
export class DeleteFoldersDto {
    
    @IsString()
    @IsNotEmpty()
    readonly id

}