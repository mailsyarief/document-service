import { MaxLength, IsNotEmpty, IsEmail, IsString, Matches, IsNumber, IsBoolean } from 'class-validator';
export class FoldersDto {
    
    @IsString()
    @IsNotEmpty()
    readonly id

    @IsString()
    @IsNotEmpty()
    readonly name
    
    readonly type

    @IsBoolean()
    @IsNotEmpty()
    readonly is_public

    @IsNumber()
    @IsNotEmpty()
    readonly timestamp
    
    readonly owner_id
    
    readonly company_id

}