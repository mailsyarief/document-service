import { MaxLength, IsNotEmpty, IsEmail, IsString, Matches, IsNumber, IsDate, IsObject, IsArray, IsBoolean } from 'class-validator';
export class DocumentsDto {


    @IsString()
    @IsNotEmpty()
    readonly id

    @IsString()
    @IsNotEmpty()
    readonly name

    @IsString()
    @IsNotEmpty()
    readonly type

    @IsString()
    @IsNotEmpty()
    readonly folder_id

    @IsObject()
    @IsNotEmpty()
    readonly content

    @IsNumber()
    @IsNotEmpty()
    readonly timestamp

    @IsBoolean()
    @IsNotEmpty()
    readonly is_public

    owner_id

    @IsArray()
    @IsNotEmpty()
    readonly share

    @IsNumber()
    readonly company_id

}