import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, Mongoose, Types } from 'mongoose';
import { Documents, DocumentsSceheme } from './document.schema';


@Schema()
export class Folders extends Document {
    @Prop({ unique: true })
    id: string;

    @Prop()
    name: string;

    @Prop()
    type: string;

    @Prop()
    is_public: boolean;

    @Prop()
    timestamp: number;

    @Prop()
    owner_id: number;

    @Prop()
    company_id: number;

}

export const FoldersSceheme = SchemaFactory.createForClass(Folders);