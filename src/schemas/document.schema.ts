import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';


@Schema()
export class Documents extends Document {
    @Prop({ unique: true })
    id: string;

    @Prop()
    name: string;

    @Prop()
    type: string;

    @Prop()
    folder_id: string;

    @Prop({ type: Object })
    content: Object;

    @Prop()
    is_public: boolean;

    @Prop()
    timestamp: number;

    @Prop()
    owner_id: number;

    @Prop()
    share: [number];

    @Prop()
    company_id: number;

}

export const DocumentsSceheme = SchemaFactory.createForClass(Documents);