import { Body, Controller, Delete, Get, Post, Put, Res, Req, Param, UseInterceptors, CacheInterceptor, Inject } from '@nestjs/common';
import { DocumentService } from '../service/document.service';
import { FolderService } from '../service/folder.service';
import { JwtService } from '../service/jwt.service';
import { HttpService } from '../service/http.service';

import { DocumentsDto } from '../dto/Documents.dto';
import { FoldersDto } from '../dto/Folders.dto';
import { DeleteFoldersDto } from '../dto/DeleteFolder.dto';
import { DeleteDocumentDto } from '../dto/DeleteDocument.dto';
import { doc } from 'prettier';

@UseInterceptors(CacheInterceptor)
@Controller('document-service')
export class AppController {
  constructor(
    private documentService: DocumentService,
    private folderService: FolderService,
    private httpService: HttpService,
    private jwtService: JwtService,
    @Inject('CACHE_MANAGER') private cacheManager
  ) { }

  @Get()
  async getDocuments(@Req() req, @Res() res) {
    const payload = await this.jwtService.payload(req)

    const cache = await this.cacheManager.get(payload['sub'] + payload['iat'] + req.path);
    if (cache) return this.httpService.ok(res, "All Data from cache", cache)

    const documents = await this.documentService.findByCompanyId(payload['company_id'])
    const folder = await this.folderService.findByCompanyId(payload['company_id'])

    const merged = [...folder, ...documents]

    const filtered = merged.filter((e) => {
      if (e.is_public) return e
      if (!e.is_public && e.owner_id == payload['owner_id']) return e
    })

    await this.cacheManager.set(payload['sub'] + payload['iat'] + req.path, filtered, { ttl: 500 });
    return this.httpService.ok(res, "All Data", filtered)
  }

  @Get('/folder/:folder_id')
  async getDetailFolder(@Req() req, @Res() res, @Param() param) {
    const payload = await this.jwtService.payload(req)
    const { folder_id } = param

    const cache = await this.cacheManager.get(payload['sub'] + payload['iat'] + req.path + folder_id);
    if (cache) return this.httpService.ok(res, "Success get folder from Cache", cache)

    const folder = await this.documentService.findByFolderId(folder_id)

    await this.cacheManager.set(payload['sub'] + payload['iat'] + req.path + folder_id, folder, { ttl: 500 });
    return this.httpService.ok(res, "Success get folder", folder)
  }

  @Get('/document/:document_id')
  async getDetailDocument(@Req() req, @Res() res, @Param() param) {
    const payload = await this.jwtService.payload(req)
    const { document_id } = param

    const cache = await this.cacheManager.get(payload['sub'] + payload['iat'] + req.path + document_id);
    if (cache) return this.httpService.ok(res, "Success get document from Cache", cache)

    const document = await this.documentService.findById(document_id)
    if (!document) return this.httpService.notFound(res, "Document Not Found", cache)
    
    await this.cacheManager.set(payload['sub'] + payload['iat'] + req.path + document_id, document, { ttl: 500 });
    return this.httpService.ok(res, "Success get document", document)
  }

  @Post('/document')
  async createDocument(@Req() req, @Res() res, @Body() documentsDto: DocumentsDto) {
    const payload = await this.jwtService.payload(req)

    const folderExist = await this.folderService.findById(documentsDto.folder_id)
    if (!folderExist) return this.httpService.notFound(res, "Folder Not Found", null)

    const documentExist = await this.documentService.findById(documentsDto.id)
    if (documentExist) {
      await this.documentService.updateDocument(documentsDto)
      await this.cacheManager.reset();
      return this.httpService.created(res, "Updated", null)
    }

    if (documentsDto.owner_id == null) {
      documentsDto.owner_id = Number(payload['user_id'])
    }

    const document = await this.documentService.insertDocument(documentsDto)
    await this.cacheManager.reset();
    return this.httpService.created(res, "Created", document)
  }

  @Post('/folder')
  async createFolder(@Req() req, @Res() res, @Body() foldersDto: FoldersDto) {
    const payload = await this.jwtService.payload(req)

    const existFolder = await this.folderService.findById(foldersDto.id)
    if (existFolder) {
      await this.folderService.updateFolder(foldersDto)
      await this.cacheManager.reset();
      return this.httpService.created(res, "Updated", null)
    }

    const folderForm = {
      id: foldersDto.id,
      name: foldersDto.name,
      type: 'folder',
      is_public: foldersDto.is_public,
      timestamp: foldersDto.timestamp,
      owner_id: payload['user_id'],
      company_id: payload['company_id'],
    }

    const newFolder = await this.folderService.insertFolder(folderForm)
    await this.cacheManager.reset();
    return this.httpService.created(res, "Created", newFolder)
  }

  @Delete('/folder')
  async deleteFolder(@Req() req, @Res() res, @Body() deleteFoldersDto: DeleteFoldersDto) {
    const deleted = await this.folderService.deleteFolder(deleteFoldersDto)
    if (!deleted.n) return this.httpService.notFound(res, "Folder Not Found", null)

    await this.cacheManager.reset();

    return this.httpService.ok(res, "Success delete folder", null)
  }

  @Delete('/document')
  async deleteDocument(@Req() req, @Res() res, @Body() deleteDocumentDto: DeleteDocumentDto) {

    const deleted = await this.documentService.deleteDocument(deleteDocumentDto)
    if (!deleted.n) return this.httpService.notFound(res, "Document Not Found", null)

    await this.cacheManager.reset();

    return this.httpService.ok(res, "Success delete document", null)
  }


}
