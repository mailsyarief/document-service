import { MiddlewareConsumer, Module, NestModule, CacheModule } from '@nestjs/common';
import { AppController } from './controller/app.controller';
import { DocumentService } from './service/document.service';
import { FolderService } from './service/folder.service';
import { HttpService } from './service/http.service';
import { JwtService } from './service/jwt.service';
import { AuthMiddleware } from './middleware/auth.middleware';
import { MongooseModule } from '@nestjs/mongoose';
import { Documents, DocumentsSceheme } from './schemas/document.schema';
import { Folders, FoldersSceheme } from './schemas/folder.schema';
import * as redisStore from 'cache-manager-redis-store';
import * as dotenv from 'dotenv';

dotenv.config();

@Module({
  imports: [
    CacheModule.register({
      store: redisStore,
      host: process.env.REDIS_HOST,
      port: process.env.REDIS_PORT,
    }),
    MongooseModule.forRoot(process.env.MONGO_DB_URL),
    MongooseModule.forFeature([
      { name: Documents.name, schema: DocumentsSceheme },
      { name: Folders.name, schema: FoldersSceheme },
    ]),
  ],
  controllers: [AppController],
  providers: [DocumentService, FolderService, HttpService, JwtService],
})

export class AppModule implements NestModule {
  configure(middleware: MiddlewareConsumer) {
    middleware.apply(AuthMiddleware).forRoutes('*')
  }
}
