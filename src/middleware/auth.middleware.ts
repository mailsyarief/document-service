
import { HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { JwtService } from '../service/jwt.service'
import { HttpService } from '../service/http.service'

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    constructor(private jwtService: JwtService, private httpService: HttpService) { }

    use(req: Request, res: Response, next: NextFunction) {
        let { authorization } = req.headers
        let decoded = this.jwtService.verify(authorization)

        if (!decoded) {
            return this.httpService.unauthorized(res, "Token Salah", null)
        }

        next();
    }
}
