import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class JwtService {
    verify(authorization) {
        try {
            return jwt.verify(authorization.replace('Bearer ', ''), process.env.PRIVATE_KEY)
        } catch (error) {
            return false
        }
    }

    payload(req) {
        let { authorization } = req.headers
        return jwt.verify(authorization.replace('Bearer ', ''), process.env.PRIVATE_KEY)
    }
}
