import { HttpException, HttpStatus, Injectable, HttpCode } from '@nestjs/common';

@Injectable()
export class HttpService {

    ok(res, message, data) {
        return res.status(HttpStatus.OK).json(
            {
                error: false,
                message: message,
                data: data,
            }
        )
    }

    created(res, message, data) {
        return res.status(HttpStatus.CREATED).json(
            {
                error: false,
                message: message,
                data: data,
            }
        )
    }

    notFound(res, message, data) {
        return res.status(HttpStatus.NOT_FOUND).json(
            {
                error: true,
                message: message,
                data: data,
            }
        )
    }

    unauthorized(res, message, data) {
        return res.status(HttpStatus.UNAUTHORIZED).json(
            {
                error: true,
                message: message,
                data: data,
            }
        )
    }

}
