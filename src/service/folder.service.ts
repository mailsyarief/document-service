import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Folders } from '../schemas/folder.schema';

import { FoldersDto } from '../dto/Folders.dto';
import { DeleteFoldersDto } from '../dto/DeleteFolder.dto';

@Injectable()
export class FolderService {

  constructor(
    @InjectModel(Folders.name) private foldersModel: Model<Folders>,
  ) { }

  getHello(): string {
    return 'Hello World!';
  }

  findByCompanyId(company_id: number) {
    return this.foldersModel.find({ company_id: company_id }).select({ "_id": 0, "__v": 0 })
  }

  findById(folder_id: string) {
    return this.foldersModel.findOne({ id: folder_id })
      .select({ "_id": 0, "__v": 0 })
  }

  updateFolder(foldersDto: FoldersDto) {
    return this.foldersModel.updateOne({ id: foldersDto.id }, foldersDto)
  }

  insertFolder(foldersDto: FoldersDto) {
    const newFolder = new this.foldersModel(foldersDto);
    return newFolder.save();
  }

  deleteFolder(deleteFoldersDto: DeleteFoldersDto) {
    return this.foldersModel.deleteOne({ id: deleteFoldersDto.id })
  }

}
