import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Documents } from '../schemas/document.schema';
import { DocumentsDto } from '../dto/Documents.dto';
import { DeleteDocumentDto } from '../dto/DeleteDocument.dto';

@Injectable()
export class DocumentService {

  constructor(
    @InjectModel(Documents.name) private documentsModel: Model<Documents>,
  ) { }

  getHello(): string {
    return 'Hello World!';
  }

  findByCompanyId(company_id: number) {
    return this.documentsModel.find({ company_id: company_id }).select({ "_id": 0, "__v": 0 })
  }

  findById(document_id: string) {
    return this.documentsModel.findOne({ id: document_id }).select({ "_id": 0, "__v": 0 })
  }

  findByFolderId(folder_id: string) {
    return this.documentsModel.findOne({ folder_id: folder_id }).select({ "_id": 0, "__v": 0 })
  }

  insertDocument(documentsDto: DocumentsDto) {
    const newDocument = new this.documentsModel(documentsDto);
    return newDocument.save();
  }

  deleteDocument(deleteDocumentDto: DeleteDocumentDto) {
    return this.documentsModel.deleteOne({ id: deleteDocumentDto.id })
  }

  updateDocument(documentsDto: DocumentsDto) {
    return this.documentsModel.updateOne({ id: documentsDto.id }, documentsDto)
  }

  getDocuments() {
    return this.documentsModel.find({}).select({ "_id": 0, "content": 0, "__v": 0 })
  }

}
